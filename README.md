# Delfosti Test
Prueba técnico para el puesto de Backend Developer

### Instalación 🔧
_Via Composer_

```
git clone https://gitlab.com/alejandro_rafael/delfosti-test.git
cd delfosti-test && composer install
php artisan serve
```
Nota: Debe de crear la base de datos manualmente y que el motor de bd sea postgreSQL

_Via Docker_

```
git clone https://gitlab.com/alejandro_rafael/delfosti-test.git
cd delfosti-test
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```

```
./vendor/bin/sail up -d
```

### Correr migraciones

_Via Composer_
```
php artisan migrate --seed
```

_Via Docker_
```
./vendor/bin/sail artisan migrate --seed
```

## Endpoints

### GET
`obtener todos los artículos`
```
/api/v1/articles
```

`obtener un solo artículo`
```
/api/v1/articles/{article_id}
```

`buscar artículos por nombre de artículo, nombre de categoría y descripción`
```
/api/v1/articles/search?article={article_name}&category={category_name}&description={article_description}
```
