<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;

Route::prefix('/v1')->group(function () {
   Route::prefix('/articles')->controller(ArticleController::class)->name('articles.')->group(function () {
       Route::get('/', 'index')->name('index');
       Route::get('/search', 'search')->name('search');
       Route::get('/{article}', 'show')->name('show');
   });
});
