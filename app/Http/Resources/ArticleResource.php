<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getRouteKey(),
            'attributes' => [
                'name'          => $this->name,
                'description'   => $this->description,
                'status'        => $this->status,
                'categories'    => $this->categories->map(function ($category) {
                    return [
                        'name'  => $category->name,
                    ];
                }),
            ],
            'links' => [
                'self'  => route('articles.show', $this->resource)
            ]
        ];
    }
}
