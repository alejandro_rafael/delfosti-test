<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleSearchRequest;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Services\Articles\SearchArticle;

class ArticleController extends Controller
{
    public function index()
    {
        return ArticleCollection::make(Article::all());
    }

    public function show(Article $article)
    {
        return ArticleResource::make($article);
    }

    public function search(ArticleSearchRequest $request)
    {
        $articles = new SearchArticle($request->article, $request->category, $request->description);
        return ArticleCollection::make($articles->__invoke());
    }
}
