<?php

namespace App\Services\Articles;

use App\Models\Article;

class SearchArticle
{
    protected $articles;

    public function __construct(
        protected ?string $article,
        protected ?string $category,
        protected ?string $description
    )
    {
    }

    public function __invoke()
    {
        $this->articles = Article::select('id', 'name', 'description', 'status');
        $this->toArticleName();
        $this->toCategoryName();
        $this->toArticleDescription();
        return $this->get();
    }

    private function toArticleName()
    {
        if ($this->article !== null) {
            $this->articles->where('name', 'like', '%' . $this->article . '%');
        }
    }

    private function toCategoryName()
    {
        if ($this->category !== null) {
            $this->articles->whereHas('categories', function ($q) {
                $q->where('name', 'like', '%' . $this->category . '%');
            });
        }
    }

    private function toArticleDescription()
    {
        if ($this->description !== null) {
            $this->articles->where('description', 'like', '%' . $this->description . '%');
        }
    }

    private function get()
    {
        return $this->articles->get();
    }
}
